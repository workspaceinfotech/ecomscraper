@foreach ($productList as $product)
    <div class="col-sm-6 col-md-3"  style="height: 400px;" data-id="{{$product->identifier}}">
        <div class="thumbnail" style="height: 300px;" >
            <a href="{{$product->details_url}}" target="_blank"><img style="height: 100%" class="img-responsive" src="{{$product->image}}" alt="No Image"></a>
            <div class="caption">
                <p style="font-size:10px; "><a href="{{$product->details_url}}" target="_blank">{{$product->title}}</a></p>
                <p>{{$product->price}}</p>
            </div>
        </div>
    </div>
    @endforeach
    </div>
    {{ $productList->render() }}