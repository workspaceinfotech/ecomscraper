<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    </head>
    <body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand home_clk" href="{{url('/')}}">Home</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <form class="navbar-form navbar-left">
                    <div class="form-group">
                        <input type="text" id="product_search"  class="form-control" placeholder="Search Your product">
                    </div>
                    {{--<button type="submit" class="btn btn-default">Submit</button>--}}
                    <select class="form-control" id="site">
                        <option value="">Sort By Store</option>
                    @foreach ($siteList as $site)
                            <option <?php if ($site->id==$siteId):?> selected="selected" <?php endif;?> value="{{$site->id}}">{{$site->name}}</option>
                        @endforeach
                    </select>

                    <select class="form-control" id="category_id">
                        <option value="">Sort By Category</option>
                    @foreach ($categoryList as $category)
                            <option <?php if ($category->id==$categoryId):?> selected="selected" <?php endif;?> value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>


                    <select class="form-control" id="subs_category" style="display: none">

                    </select>

                </form>




            </div><!-- /.navbar-collapse -->

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
        <div class="container">
            <div class="row" id="products_content">
               @foreach ($productList as $product)
                <div class="col-sm-6 col-md-3"  style="height: 400px;" data-id="{{$product->identifier}}">
                    <div class="thumbnail" style="height: 300px;" >
                        <a href="{{$product->details_url}}" target="_blank"><img style="height: 100%" class="img-responsive" src="{{$product->image}}" alt="No Image"></a>
                        <div class="caption">
                            <p style="font-size:10px; "><a href="{{$product->details_url}}" target="_blank">{{$product->title}}</a></p>
                            <p>{{$product->price}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
                   {{ $productList->render() }}
            </div>

        </div>

    <button type="button" style="display: none;" class="btn btn-info btn-lg modal_click" data-toggle="modal" data-target="#myModal">Open Modal</button>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Alert!</h4>
                </div>
                <div class="modal-body">
                    <p id="alert-txt"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>




    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="application/javascript">

        $(function()
        {

            $('html').keyup(function(e){
                if(e.keyCode == 8){

                    if($("#product_search").val()==""){
                        window.location=$(".home_clk").attr("href");
                    }
                }



            });
            $( "#product_search" ).autocomplete({
                source: "autocomplete",
                minLength: 2,
                select: function(event, ui) {
                    $('#product_search').val(ui.item.value);

                    $.ajax({
                        url : 'autocomplete_search',
                        type : 'GET',
                        data : {
                            'term' : ui.item.value
                        },
                        dataType:'html',
                        success : function(data) {
                            $("#products_content").html(data);
                            $("option:selected").removeAttr("selected");
                        },
                        error : function(request,error)
                        {
                        }
                    });

                }
            });



            $("#site").change(function () {

                if($("#site").val()!="")
                    $("option:selected").removeAttr("selected");
                     $( "select#category_id" ).val("");
                     $( "select#subs_category" ).val("");
                $.ajax({
                        url : 'sitesearch',
                        type : 'GET',
                        data : {
                            'site' : $("#site").val()
                        },
                        dataType:'html',
                        success : function(data) {
                            $("#products_content").html(data);
                        },
                        error : function(request,error)
                        {
                        }
                    });

            });


            $("#category_id").change(function () {


                if( $("#site").val()==""){
                    $("#alert-txt").text("Please,Select your store.");
                    $(".modal_click").trigger("click");
                    return false;
                }

                if($("#category_id").val()!="")
                    $.ajax({
                        url : 'categorySearch',
                        type : 'GET',
                        data : {
                            'category_id' :  $("#category_id").val(),
                            'site' :  $("#site").val()
                        },
                        dataType:'html',
                        success : function(data) {
                            $("#products_content").html(data);


                            $.ajax({
                                url : 'getsubscategory',
                                type : 'GET',
                                data : {
                                    'category_id' :  $("#category_id").val(),
                                    'site' :  $("#site").val()
                                },
                                dataType:'html',
                                success : function(data) {
                                    $("#subs_category").html(data);
                                    $("#subs_category").show();


                                    $("#subs_category").change(function () {

                                        if($("#subs_category").val()!=""){

                                            $.ajax({
                                                url : 'product_by_subs_cate',
                                                type : 'GET',
                                                data : {
                                                    'sub_category' : $("#subs_category").val(),
                                                },
                                                dataType:'html',
                                                success : function(data) {
                                                    $("#products_content").html(data);
                                                },
                                                error : function(request,error)
                                                {
                                                }
                                            });


                                        }
                                    });



                                },
                                error : function(request,error)
                                {
                                }
                            });

                        },
                        error : function(request,error)
                        {
                        }
                    });

            });

        });

    </script>

</html>
