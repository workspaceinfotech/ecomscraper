<?php
/**
 * Created by PhpStorm.
 * User: Sarwar
 * Date: 3/29/2017
 * Time: 6:42 AM
 */

namespace App\Http\Controllers;


use App\Category;
use App\Product;
use App\Site;
use App\Stores;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    public function __construct()
    {

    }

    public function index(){

        $productObj=new Product();
        $storeObj=new Stores();
        $siteObj=new Site();
        $cateObj=new Category();

        #$productList=$productObj->getProducts();
        $seed = rand(1,99999);
        $productList=$productObj
            ->orderByRaw(DB::raw('RAND('.$seed.')'))
            ->paginate(32);

        $storeList=$storeObj->all();
        $siteList=$siteObj->all();
        $categoryList=$cateObj->all();


        if(Session::has('category_id') && Session::has('site')){

            $category_id= Input::get('category_id');
            $site = Input::get('site');

            $subs_category = $productObj
                ->where('site_id', $site)
                ->where('category_id', $category_id)
                ->paginate(32);


        }



         return view("welcome",$viewBag=array(
                'productList'=>$productList,
                'storeList'=>$storeList,
                'siteList'=>$siteList,
                'categoryList'=>$categoryList,
                'storeId'=>0,
                'siteId'=>0,
                'categoryId'=>0

         ));
    }


    public function autocomplete(){

        $productObj=new Product();

        $term = Input::get('term');

        $results = array();

        $queries = $productObj
            ->where('title', 'LIKE', '%'.$term.'%')
            #->orWhere('title', 'LIKE', '%'.$term.'%')
            ->paginate(32);

        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'value' => $query->title];
        }

        return Response::json($results);
    }

    public function autocompleteSearch(){

        $productObj=new Product();

        $term = Input::get('term');

        $results = array();

        $productList = $productObj
            ->where('title', 'LIKE', '%'.$term.'%')
            #->orWhere('title', 'LIKE', '%'.$term.'%')
            ->paginate(32);



        return view("search",$viewBag=array(
            'productList'=>$productList
        ));
    }




    public function siteSearch(){

        $productObj=new Product();
        $storeObj=new Stores();
        $siteObj=new Site();
        $cateObj=new Category();


        $storeList=$storeObj->all();
        $siteList=$siteObj->all();
        $categoryList=$cateObj->all();


        if(Input::get('site')){
            $site = Input::get('site');

            $results = array();

            $productList = $productObj
                ->where('site_id', $site)
                ->paginate(32);

            Session::set('site', $site);


            return view("search",$viewBag=array(
                'productList'=>$productList,
                'storeList'=>$storeList,
                'siteList'=>$siteList,
                'categoryList'=>$categoryList,
                'storeId'=>0,
                'siteId'=>$site,
                'categoryId'=>0

            ));



        }else{

            $site=Session::get('site');
            $productList = $productObj
                ->where('site_id', $site)
                ->paginate(32);

            $storeList=$storeObj->all();

            return view("welcome",$viewBag=array(
                'productList'=>$productList,
                'storeList'=>$storeList,
                'siteList'=>$siteList,
                'categoryList'=>$categoryList,
                'storeId'=>0,
                'siteId'=>$site,
                'categoryId'=>0

            ));


        }



    }


    public function categorySearch()
    {

        $productObj=new Product();
        $storeObj=new Stores();
        $siteObj=new Site();
        $cateObj=new Category();


        $storeList=$storeObj->all();
        $siteList=$siteObj->all();
        $categoryList=$cateObj->all();

        if (Input::get('category_id')) {
            $category_id = Input::get('category_id');
            $site = Input::get('site');

            $results = array();

            $productList = $productObj
                ->where('site_id', $site)
                ->where('category_id', $category_id)
                ->paginate(32);

            Session::set('category', $category_id);



            return view("search", $viewBag = array(
                'productList' => $productList,
            ));


        } else {

            $category_id = Session::get('category');
            $site = Session::get('site');
            $productList = $productObj
                ->where('site_id', $site)
                ->where('category_id', $category_id)
                ->paginate(32);

            $storeList = $storeObj->all();


            return view("welcome", $viewBag = array(
                'productList'=>$productList,
                'storeList'=>$storeList,
                'siteList'=>$siteList,
                'categoryList'=>$categoryList,
                'storeId'=>0,
                'siteId'=>$site,
                'categoryId'=>$category_id
            ));
        }


    }

    public function getsubscategory(){

        $productObj = new Product();

        $storeObj = new Stores();

        $category_id = Session::get('category');
        $site = Session::get('site');

        $subscateList = $productObj
            ->select('sub_category_name')
            ->where('site_id', $site)
            ->where('category_id', $category_id)
            ->distinct('sub_category_name')
            ->get();

        return view("option", $viewBag = array(
            'subscateList' => $subscateList,

        ));
    }

    public function product_by_subs_cate(){

        $productObj=new Product();
        $storeObj=new Stores();
        $siteObj=new Site();
        $cateObj=new Category();

        $storeList=$storeObj->all();
        $siteList=$siteObj->all();
        $categoryList=$cateObj->all();


        $category_id = Session::get('category');
        $site = Session::get('site');

        $sub_category = Input::get('sub_category');

        $category_id = Session::get('category');
        $site = Session::get('site');
        $productList = $productObj
            ->where('site_id', $site)
            ->where('category_id', $category_id)
            ->where('sub_category_name', $sub_category)
            ->paginate(300);

        $storeList = $storeObj->all();


        return view("search", $viewBag = array(
            'productList' => $productList,
            'storeList' => $storeList,
            'category_id' => $category_id
        ));


    }




}