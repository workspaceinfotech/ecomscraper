<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'ProductController@index');

Route::get('/autocomplete', 'ProductController@autocomplete');
Route::get('/autocomplete_search', 'ProductController@autocompleteSearch');
Route::get('/categorySearch', 'ProductController@categorySearch');
Route::get('/sitesearch', 'ProductController@siteSearch');
Route::get('/autocompletesubcategory', 'ProductController@autocomplete_sub_category');
Route::get('/getsubscategory', 'ProductController@getsubscategory');
Route::get('/product_by_subs_cate', 'ProductController@product_by_subs_cate');

